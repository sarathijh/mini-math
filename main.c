#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

typedef enum { NONE, DONE, NUM, LPAR='(', RPAR=')', 
               ADD='+', SUB='-', MUL='*', DIV='/' } tok_kind;
typedef struct { tok_kind kind; int64_t num; } tok_t;
struct node { tok_t tok; struct node *l; struct node *r; };
typedef struct node node;

FILE *in;

tok_t prev = (tok_t){NONE};
void  ungettok(tok_t *tok){ prev = *tok; }
tok_t gettok() {
  if (prev.kind != NONE){ tok_t ret = prev; prev.kind = NONE; return ret; }
  int c;
  while (isspace (c = fgetc(in)) && c != '\n'){}
  switch (c) {
    case '+': return (tok_t){ADD};  case '-': return (tok_t){SUB};
    case '*': return (tok_t){MUL};  case '/': return (tok_t){DIV};
    case '(': return (tok_t){LPAR}; case ')': return (tok_t){RPAR};
    case '\n': return (tok_t){DONE};
    default: {
      int64_t num = 0;
      int i = 0;
      for (; i < 11 && c >= '0' && c <= '9'; ++i) {
        if (i == 10) {
          printf("Integer overflow: %llu...\n", num);
          return (tok_t){DONE};
        }
        num = 10*num + (c-'0');
        c = fgetc(in);
      }
      if (i > 0) { ungetc(c, in); return (tok_t){NUM, num}; }
    }
  }
  printf("Unknown character '%c'\n", c);
  return (tok_t){DONE};
}

/**
 * expr -> term '+' expr | term '-' expr | term
 * term -> atom '*' term | atom '/' term | atom
 * atom -> '(' expr ')' | 'num'
 */

node* make_node(const tok_t *tok, node *l, node *r) {
  node *n = (node*)malloc(sizeof(node));
  n->tok = *tok;
  n->l=l; n->r=r;
  return n;
}

node* parse_expr();

node* parse_atom() {
  tok_t tok = gettok();
  if (tok.kind == NUM)  { return make_node(&tok, 0, 0); }
  if (tok.kind == LPAR) {
    node *expr = parse_expr();
    if (gettok().kind != RPAR){ printf("Expected ')'\n"); return 0; }
    return expr;
  }
  printf("Expected number or '('\n"); return 0;
}

node* parse_term() {
  node *atom = parse_atom(); if (!atom){ return 0; }
  tok_t tok = gettok();
  if (tok.kind != MUL && tok.kind != DIV){ ungettok(&tok); return atom; }
  return make_node(&tok, atom, parse_term());
}

node* parse_expr() {
  node *term = parse_term(); if (!term){ return 0; }
  tok_t tok = gettok();
  if (tok.kind != ADD && tok.kind != SUB){ ungettok(&tok); return term; }
  return make_node(&tok, term, parse_expr());
}

void print_tree(node *tree) {
  if (!tree){ return; }
  if (tree->tok.kind == NUM) { printf("%llu", tree->tok.num); } else {
    printf("%c", tree->tok.kind);
    printf("(");
    print_tree(tree->l);
    printf(", ");
    print_tree(tree->r);
    printf(")");
  }
}

int main(int argc, char **argv) {
  in = stdin;
  node *tree = parse_expr();
  print_tree(tree);
  printf("\n");
  return 0;
}

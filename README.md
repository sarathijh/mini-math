# README #

A simple LL(1) recursive descent mathematical expression parser written in C using less than 100 lines of code.

Run the program, enter an expression consisting of 64 bit integers and any of the following symbols: ['+', '-', '*', '/', '(', ')']. Spaces are ignored.


# GRAMMAR #

* expr -> term '+' expr | term '-' expr | term
* term -> atom '*' term | atom '/' term | atom
* atom -> '(' expr ')' | 'num'


# ISSUES #

Issue: Grammar is right-associative. Solution: rewrite as left-associative.